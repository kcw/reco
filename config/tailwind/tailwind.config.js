/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/app.html',
    './src/**/*.{html,js,tsx,svelte}'
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}
